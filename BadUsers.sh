#!/bin/bash
p=0
usage="Usage: BadUser.sh [-p] / [-t N(d/m/y)]"
# detecció de opcions d'entrada: només son vàlids: sense paràmetres i -p 
if [ $# -ne 0 ]; then
    if [ $# -eq 1 ];then
        if [ $1 == "-p" ]; then
            p=1
        else
            echo $usage; exit 1
       	fi
    elif [ $# -eq 2 ];then
        if [ $1 == "-t" ]; then
            p=2
            if [ ${2: -1} == "d"  ]; then
                mtime=${2:0:-1}
                ntime="-${2:0:-1}days"
			      elif [ ${2: -1} == "m"  ]; then
				        mtime=$((${2:0:-1} * 30))
				        ntime="-${2:0:-1}month"
			      elif [ ${2: -1} == "y"  ]; then
				        mtime=$((${2:0:-1} * 365))
				        ntime="-${2:0:-1}years"
			      else
				        echo $usage; exit 1
			      fi
        else
		        echo $usage; exit 1
       	fi
    else
	      echo $usage; exit 1
    fi 
fi

#IFS=$`\n`
# afegiu una comanda per llegir el fitxer de password i només agafar el camp de
# nom de l'usuari
for user in $(cat /etc/passwd | cut -d: -f1); do
	  home=`cat /etc/passwd | grep "^$user\>" | cut -d: -f6`
	  if [ -d $home ]; then
		    num_fich=`find $home -type f -user $user 2>/dev/null| wc -l`
	  else
		    num_fich=0
    fi

	  if [ $num_fich -eq 0 ] ; then
		    if [ $p -eq 1 ]; then
			      # afegiu una comanda per detectar si l'usuari te processos en exec
            # si no te ningú la variable $user_proc ha de ser 0
			      user_proc=`ps -u $user | tail -n +2 | wc -l`
			      if [ $user_proc -eq 0 ]; then
				        echo "$user"
			      fi 
		    else
			      echo "$user"
		    fi
	  else
		    if [ $p -eq 2 ]; then
			      # Calculem els dies de modificacio
			      mod=`find $home -type f -user $user -mtime $mtime | wc -l`
			      lastlog=`last -w $user -s $ntime | head -n -3 | wc -l`
			      if [ $mod -eq 0 ] && [ $lastlog -eq 0 ] ; then
				        echo $user
			      fi
		    fi
	  fi
done
