#!/bin/bash
accCap=0
usage="Usage: ocupacio.sh [-g grupname] n(M/K/B)"
missatge="echo -e \"Missatge ocupacio: T estas flipan amb la cap\nEliminar missatge: (sed -i '/ocupacio/d' ~/.bash_profile)\" "

# detecció de opcions d'entrada
if [ $# -ne 0 ]; then

	if [ $# -eq 1 ];then
		info=$1
		users=$(cat /etc/passwd | cut -d: -f1)

	elif [ $# -eq 3 ];then
		if [ $1 == "-g" ]; then
			users=$(cut -d: -f1,4 /etc/passwd | grep $(getent group $2 | cut -d: -f3)$ | cut -d: -f1)
			info=$3
		else
			echo $usage; exit 1
		fi

	else
		echo $usage; exit 1
       	fi


	#Transformem el input en K per tal d'utilitzar el du
	if [ ${info: -1} == "K"  ]; then
		readCap=${info:0:-1}
	elif [ ${info: -1} == "M"  ]; then
		readCap=$((${info:0:-1} * 1024))
	elif [ ${info: -1} == "G"  ]; then
		readCap=$((${info:0:-1} * 1024 * 1024))
	else
		echo $usage; exit 1
	fi

else
	echo $usage; exit 1
fi


#IFS=$`\n`
# afegiu una comanda per llegir el fitxer de password i només agafar el camp de # nom de l'usuari
for user in $users; do
	home=`cat /etc/passwd | grep "^$user\>" | cut -d: -f6 | grep "\home"`
  if [ -d "$home" ]; then
      cap=`du -s $home | cut -f -1`
      if [ $cap -gt  $readCap ]; then
      		echo "MAL :USER: $user / HOME: $home / CAP: $cap / READCAP: $readCap"
          echo $missatge >> "$home/.bash_profile"
      else
          echo "BO :USER: $user / HOME: $home / CAP: $cap / READCAP: $readCap"
      fi
      ((accCap+=$cap))
  fi
done

if [ $1 == "-g" ];then
	echo $accCap
fi
