#!/bin/bash
usage="infouser.sh user"
if [ $# -eq 1 ];then
	home=$(cat /etc/passwd | grep "^$1\>" | cut -d: -f6)
	size=$(du -hs $home | awk '{print $1;}')
	ps=$(ps -U $1 | tail -n +2 | wc -l)
	dir=$(find /  \( -path $home -o -path /run -o -path /sys -o -path /proc \) -prune -o -type d -user $1 -print)
	echo "Home: $home"
	echo "Home size: $size"
	echo "Other dirs: $dir"
	echo "Active processes: $ps"
else
	echo $usage; exit 1
fi
