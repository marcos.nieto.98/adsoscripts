#!/bin/bash
paq_total=0
while :
do
	for user in $(netstat -i | cut -d ' ' -f1 | tail -n +3); do
		iptables=$(netstat -i | grep "^$user\>" | awk '{print $7;}');
		echo "$user:	$iptables"
		((paq_total+=iptables))
	done

	echo "Total:	$paq_total"
	paq_total=0
	sleep $1
done
